// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.21;

import {Script, console2} from "forge-std/Script.sol";
import "../lib/forge-std/src/StdJson.sol";
import {IERC20} from "@openzeppelin/contracts/token/ERC20/IERC20.sol";

import {DeTrustFactory} from "../src/DeTrustFactory.sol";
import {DeTrustModel_01_Executive} from "../src/DeTrustModel_01_Executive.sol";
import {DeTrustModel_02_Executive} from "../src/DeTrustModel_02_Executive.sol";
import {UsersDeTrustRegistry} from "../src/UsersDeTrustRegistry.sol";
import {DeTrustModelRegistry} from "../src/DeTrustModelRegistry.sol";


contract InteracteScript is Script {
    using stdJson for string;
    address _userReg = 0xaE8f9B27AE7af8267C0b9a57Fdc21110989f40ea;  //    UsersDeTrustRegistry
    address payable _impl_01 = payable(0xE0d5A171eB394B3760Bc5eEB020Fb53B96815b7b);     //    DeTrustModel_01_Executive
    address payable _impl_02 = payable(0xCe05ABB733072Bb81bc16802120729f910299Bc9);     //    DeTrustModel_02_Executive
    address payable _modelReg = payable(0x663BeF18503572FC29d7db80cb79e44A9DBC4672); //   DeTrustModelRegistry
    address payable _factory = payable(0x3DfAC2f98E47a05ef15094516fA30B780cF2baC6);  //    DeTrustFactory
    address _ubdn = 0x7ce7abb7F8794dCe67FB2dc4d8eBf2F033472730;
    address _routerV2 = 0xC532a74256D3Db42D0Bf7a0400fEFDbad7694008;
    address _weth = 0x7b79995e5f793A07Bc00c21412e50Ecae098E7f9;
    address _usdt = 0xaA8E23Fb1079EA71e0a56F48a2aA51851D8433D0;
    uint256 feeAmount = 220e18;
    uint256 etherAmount = 1e15;
    uint256 ubdnAmount = 1e18;
    uint256 withdrawERC20Amount = 1e16;
    uint256 withdrawEtherAmount = 1e12;
      
    
    DeTrustModelRegistry modelReg = DeTrustModelRegistry(_modelReg);
    UsersDeTrustRegistry userReg = UsersDeTrustRegistry(_userReg);
    DeTrustModel_01_Executive impl_01 = DeTrustModel_01_Executive(_impl_01);
    DeTrustModel_02_Executive impl_02 = DeTrustModel_02_Executive(_impl_02);
    DeTrustFactory factory = DeTrustFactory(_factory);
    
    // console2.log("factory: %s", address(factory));

    bytes32[] inheritorHashes = new bytes32[](2);
    uint256 amount = 10e18;
    uint64 silentPeriod = 10000;
    string detrustName = 'Alex trust';
    uint256 inheritedTime = block.timestamp + 10000;

    function run() public {
        console2.log("Chain id: %s", vm.toString(block.chainid));
        console2.log("Msg.sender address: %s, %s", msg.sender, msg.sender.balance);


        // Load json with chain params
        string memory root = vm.projectRoot();
        string memory params_path = string.concat(root, "/script/chain_params.json");
        string memory params_json_file = vm.readFile(params_path);
        string memory key;
        console2.log('Hi');

        // 

        inheritorHashes[0] = keccak256(abi.encode(0x110FA9c41cb43c08ad98391dFb52a9A0713d9613));
        inheritorHashes[1] = keccak256(abi.encode(0xf315B9006C20913D6D8498BDf657E778d4Ddf2c4));
        address creator = 0x5992Fe461F81C8E0aFFA95b831E50e9b3854BA0E;
        address receiver = 0xf315B9006C20913D6D8498BDf657E778d4Ddf2c4;

        vm.startBroadcast();
        // deploy trust _impl_01
        /*IERC20(_ubdn).approve(_modelReg, feeAmount);
        address _proxy = factory.deployProxyForTrust(
            _impl_01, 
            creator,  // _creator
            inheritorHashes, // _inheritorHashes
            silentPeriod,    //_silence
            detrustName     //_name
        );
        console2.log("proxy: %s", proxy);*/

        // deploy trust _impl_02
        /*IERC20(_ubdn).approve(_modelReg, feeAmount);
        address _proxy = factory.deployProxyForTrust(
            _impl_02, 
            creator,  // _creator
            inheritorHashes, // _inheritorHashes
            silentPeriod,    //_silence
            detrustName     //_name
        );
        console2.log("proxy: %s", _proxy);*/
        

        address payable _proxy = payable(0x3367e6FeCCdd7a636213AFe65babDa2A4FEEc508);
        // topup trust
        //IERC20(_ubdn).transfer(_proxy, ubdnAmount);
        //address payable _receiver = payable(_proxy);
        //_receiver.transfer(etherAmount);
        DeTrustModel_01_Executive proxy = DeTrustModel_01_Executive(_proxy);
        /*proxy.transferNative(receiver, withdrawEtherAmount);
        // pay fee 
        IERC20(_ubdn).transfer(_proxy, feeAmount * 2);
        proxy.payFeeAdvance(2);
        proxy.transferERC20(_ubdn, receiver, withdrawERC20Amount);
        proxy.iAmAlive();
        bytes memory payload = abi.encodeWithSignature("transfer(address,uint256)", receiver, withdrawERC20Amount);
        
        proxy.executeOp(_ubdn,0,payload);*/

        // swap weth to usdt
        
        address[] memory _targetArray = new address[](2);
        _targetArray[0] = _weth;
        _targetArray[1] = _routerV2;

        uint256[] memory _valueArray = new uint256[](2);
        _valueArray[0] = 0;
        _valueArray[1] = 0;

        bytes[] memory _dataArray = new bytes[](2);

        _dataArray[0] = abi.encodeWithSignature("approve(address,uint256)", _routerV2, 1e15);
        address[] memory _path = new address[](2);
        _path[0] = 0x7b79995e5f793A07Bc00c21412e50Ecae098E7f9; // weth
        _path[1] = 0xaA8E23Fb1079EA71e0a56F48a2aA51851D8433D0; // usdt
        _dataArray[1] = abi.encodeWithSignature("swapExactTokensForTokens(uint256,uint256,address[],address,uint256)",1e15,0,_path,_proxy,2*block.timestamp);

        /* add liquidity to pool (weth-usdt)
        address[] memory _targetArray = new address[](2);
        _targetArray[0] = _usdt;
        _targetArray[1] = _routerV2;

        uint256[] memory _valueArray = new uint256[](2);
        _valueArray[0] = 0;
        _valueArray[1] = 436847982635131;

        bytes[] memory _dataArray = new bytes[](2);

        _dataArray[0] = abi.encodeWithSignature("approve(address,uint256)", _routerV2, 20000);
        address[] memory _path = new address[](2);
        _path[0] = 0x7b79995e5f793A07Bc00c21412e50Ecae098E7f9; // weth
        _path[1] = 0xaA8E23Fb1079EA71e0a56F48a2aA51851D8433D0; // usdt
        _dataArray[1] = abi.encodeWithSignature("addLiquidityETH(address,uint256,uint256,uint256,address,uint256)",_usdt,20000,10000,0,_proxy,2*block.timestamp);*/
        proxy.executeMultiOp(_targetArray, _valueArray, _dataArray);

        
        /*proxy.chargeAnnualFee();
        // swap eth to usdt
        address[] memory _path = new address[](2);
        _path[0] = _weth; // weth
        _path[1] = _usdt; // usdt
        //bytes memory payload = abi.encodeWithSignature("swapExactTokensForTokens(uint256,uint256,address[],address,uint256)",1e10,0,_path,_proxy,block.timestamp);
        bytes memory payload = abi.encodeWithSignature("swapExactETHForTokens(uint256,address[],address,uint256)",0,_path,_proxy,2*block.timestamp);
        proxy.executeOp(_routerV2,1e12,payload);*/

        vm.stopBroadcast();

    }
}