// SPDX-License-Identifier: MIT
// Proxy for UBD Network
pragma solidity 0.8.23;

import {Proxy} from "@openzeppelin/contracts/proxy/Proxy.sol";

/**
 * @dev !!!NON UPGRADABLE!!! eip1967 proxy inspired from OppenZeppelin
 */
contract DeTrustProxy is Proxy {

    struct AddressSlot {
        address value;
    }
    /**
     * @dev Storage slot with the address of the current implementation.
     * This is the keccak-256 hash of "eip1967.proxy.implementation" subtracted by 1, and is
     * validated in the constructor.
     */
    bytes32 internal constant _IMPLEMENTATION_SLOT = 0x360894a13ba1a3210667c828492db98dca3e2076cc3735a920a3ca505d382bbc;

    /**
     * @dev Emitted when the implementation is upgraded.
     */
    event Upgraded(address indexed implementation);

    constructor(
        address _implAddress, 
        address _owner,
        bytes32 _inheritorHash,
        uint64  _silence,
        string memory _name
    ) 
    {
        getAddressSlot(_IMPLEMENTATION_SLOT).value = _implAddress;
        emit Upgraded(_implAddress);
        (bool success, ) = 
        _implAddress.delegatecall(
            abi.encodeWithSignature(
                "initialize(address,bytes32,uint64,string)"
                , _owner, _inheritorHash, _silence, _name
            )
        );
        require(success, "Construction failed");
    }


    /**
     * @dev Returns the current implementation address.
     */
    function _implementation() internal view virtual override returns (address impl) {
        return getAddressSlot(_IMPLEMENTATION_SLOT).value;
    }

    /**
     * @dev Returns an `AddressSlot` with member `value` located at `slot`.
     */
    function getAddressSlot(bytes32 slot) internal pure returns (AddressSlot storage r) {
        /// @solidity memory-safe-assembly
        assembly {
            r.slot := slot
        }
    }
} 