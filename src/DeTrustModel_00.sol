// SPDX-License-Identifier: MIT
// UBD Network DeTrustModel_00
pragma solidity 0.8.23;

import {IERC20} from "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import {SafeERC20} from "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import {Address} from "@openzeppelin/contracts/utils/Address.sol";
import {ContextUpgradeable} from "@Uopenzeppelin/contracts/utils/ContextUpgradeable.sol";
import {Initializable} from "@Uopenzeppelin/contracts/proxy/utils/Initializable.sol";

/**
 * @dev This is a simple trust model implementation.
 * The simplest implementation of a smart wallet with an asset transfer 
 * function. To create trust the owner must have XXX UBDN on his balance 
 * (no any fees are charged). Upon creation the address of the heir and 
 * the time (dT) of the owner’s absence(silence) are passed. When the heir 
 * applies, this time of inactivity of the owner will be checked. If it is 
 * greater than dT, then both the creator and the heir have access 
 * to the wallet's assets. 
 * 
 * !!! This is implementation contract for proxy conatract creation
 */
contract DeTrustModel_00 is 
    Initializable, 
    ContextUpgradeable 
{
    
    /// @custom:storage-location erc7201:ubdn.storage.DeTrustModel_00
    struct DeTrustModelStorage {
        bytes32 inheritorHash;
        uint256 lastOwnerOp;
        address creator;
        uint64 silenceTime;
        bool inherited;
        string name;
    }

    // keccak256(abi.encode(uint256(keccak256("ubdn.storage.DeTrustModel_00")) - 1)) & ~bytes32(uint256(0xff))
    bytes32 private constant DeTrustModelStorageLocation =  0xb4ad99c77d52a0382436420c0cefad51d59a0e2abc535826670345dbc39e4b00;    
    
    /**
     * @dev The caller account is not authorized to perform an operation.
     */
    error OwnableUnauthorizedAccount(address account);
    
    event EtherTransfer(uint256 amount);

    /**
     * @dev Throws if called by any account other than the creator 
     *  or inheritor after silence time.
     */
    modifier onlyCreatorOrInheritor() {
        _checkCreatorOrInheritor();
        _;
    }


    function initialize(
        address _creator,
        bytes32 _inheritorHash,
        uint64  _silence,
        string memory _name
    ) public initializer
    {
        __DeTrustModel_00_init(_creator, _inheritorHash, _silence, _name);

    }

    /**
     * @dev Sets the sender as the initial owner, the beneficiary as the pending owner, 
     * the start timestamp and the
     * vesting duration of the vesting wallet.
     */
    function __DeTrustModel_00_init(
        address _creator,
        bytes32 _inheritorHash,
        uint64  _silence,
        string memory _name
    ) internal onlyInitializing 
    {
        //__Ownable_init_unchained(_owner);
        __DeTrustModel_00_init_unchained(_creator, _inheritorHash, _silence, _name);
    }

    function __DeTrustModel_00_init_unchained(
        address _creator,
        bytes32 _inheritorHash,
        uint64  _silence,
        string memory _name
    ) internal onlyInitializing 
    {
        DeTrustModelStorage storage $ = _getDeTrustModel_00Storage();
        $.inheritorHash = _inheritorHash;
        $.silenceTime = _silence;
        $.creator = _creator;
        $.lastOwnerOp = block.timestamp;
        $.name = _name;
    }


    /**
     * @dev The contract should be able to receive Eth.
     */
    receive() external payable virtual {}

    /**
     * @dev Use this method for acces native token balance
     * @param _to address of receiver
     * @param _value value in wei
     */
    function transferNative(address _to, uint256 _value) 
        external
        onlyCreatorOrInheritor 
    {
        DeTrustModelStorage storage $ = _getDeTrustModel_00Storage();
        Address.sendValue(payable(_to), _value);
        _updateLastOwnerOp($);
        //$.lastOwnerOp = block.timestamp;
        emit EtherTransfer(_value);
    }

    /**
     * @dev Use this method for acces ERC20 token balance
     * @param _token address of ERC20 asset
     * @param _to address of receiver
     * @param _amount value in wei
     */
    function transferERC20(address _token, address _to, uint256 _amount) 
        external 
        onlyCreatorOrInheritor
    {
       DeTrustModelStorage storage $ = _getDeTrustModel_00Storage();
       SafeERC20.safeTransfer(IERC20(_token), _to, _amount);
       //$.lastOwnerOp = block.timestamp;
       _updateLastOwnerOp($);
    }

    /**
     * @dev Call this method for extend (reset) silnce time counter.
     */
    function iAmAlive() external onlyCreatorOrInheritor {
       DeTrustModelStorage storage $ = _getDeTrustModel_00Storage();
       _updateLastOwnerOp($);
    }

    /**
     * @dev Returns creator of DeTrus proxy
     */
    function creator() external view returns(address){
        DeTrustModelStorage storage $ = _getDeTrustModel_00Storage();
        return $.creator;
    }

    /**
     * @dev Returns full DeTrust info
     */
    function trustInfo() external view returns(DeTrustModelStorage memory trust){
        trust = _getDeTrustModel_00Storage();
    }

    ////////////////////////////////////////////////////////////////////////

    /**
     * @dev Refresh last operation timestamp
     */
    function _updateLastOwnerOp(DeTrustModelStorage storage st) internal {
        if (st.inheritorHash == keccak256(abi.encode(_msgSender()))) {
            // if code here hence time condition are OK 
            // and from this moment assete are inherited
            st.inherited = true;
        }

        if (! st.inherited) {
            // update only till inhereted moment
            st.lastOwnerOp = block.timestamp;
        }

    }

    /**
     * @dev Throws if the sender is not the owner
     *  or inheritor after silence time.
     */
    function _checkCreatorOrInheritor() internal view virtual {
        DeTrustModelStorage storage $ = _getDeTrustModel_00Storage();
        if (
                ($.creator != _msgSender() &&
                ($.inheritorHash != keccak256(abi.encode(_msgSender())))
            )
                ||
            (
                  ($.inheritorHash == keccak256(abi.encode(_msgSender())))
                && (block.timestamp < ($.lastOwnerOp + uint256($.silenceTime)))
            )
        ) {
            revert OwnableUnauthorizedAccount(_msgSender());
        }
    }

    function _getDeTrustModel_00Storage() private pure returns (DeTrustModelStorage storage $) {
        assembly {
            $.slot := DeTrustModelStorageLocation
        }
    }

}
