// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.13;

import {Test, console2, Vm} from "forge-std/Test.sol";
import "forge-std/console.sol";
import {Address} from "@openzeppelin/contracts/utils/Address.sol";

import {DeTrustFactory} from "../src/DeTrustFactory.sol";
import {DeTrustModel_01_Executive} from "../src/DeTrustModel_01_Executive.sol";
import {MockERC20} from "../src/mock/MockERC20.sol";

import {UsersDeTrustRegistry} from "../src/UsersDeTrustRegistry.sol";
import {DeTrustModelRegistry} from "../src/DeTrustModelRegistry.sol";


contract FactoryTest_m_04 is Test {
    uint256 public sendEtherAmount = 1e18;
    uint256 public receiveEtherAmount;
    uint256 public sendERC20Amount = 2e18;
    uint256 public neededERC20Amount = 100e18;
    string public detrustName = 'NameOfDeTrust';

    DeTrustFactory  public factory;
    DeTrustModel_01_Executive public impl_01;
    UsersDeTrustRegistry public userReg;
    DeTrustModelRegistry public modelReg;

    MockERC20 public erc20;

    receive() external payable virtual {}
    function rEth() external payable {
        receiveEtherAmount = msg.value;
    }

    function setUp() public {
        modelReg = new DeTrustModelRegistry(msg.sender);
        userReg = new UsersDeTrustRegistry();
        factory = new DeTrustFactory(address(modelReg), address(userReg));
        impl_01 = new DeTrustModel_01_Executive();
        erc20 = new MockERC20('UBDN Mock ERC20 Token', 'UBDN');
        modelReg.setModelState(
            address(impl_01),
            DeTrustModelRegistry.TrustModel(bytes1(0x07), address(erc20), neededERC20Amount, address(erc20), 2e18)
        );
        userReg.setFactoryState(address(factory), true);
    }

    function test_createProxy() public {
        bytes32[] memory inheritorHases = new bytes32[](2);
        inheritorHases[0] = keccak256(abi.encode(address(0)));
        inheritorHases[1] = keccak256(abi.encode(address(1)));
        //DeTrustModelRegistry.TrustModel memory m = modelReg.approvedModels(address(impl_01));
        (bytes1 r,,,,uint256 f) = modelReg.approvedModels(address(impl_01));
        erc20.approve(address(modelReg), f);
        address proxy = factory.deployProxyForTrust(
            address(impl_01), 
            msg.sender,
            inheritorHases, 
            0,
            detrustName
        );
        

        address[] memory t =  userReg.getCreatorTrusts(address(this));
        assertEq(t.length, 1);
        
        address[] memory intr =  userReg.getInheritorTrusts(address(0));
        assertEq(intr.length, 1);
    }

    function test_execOp() public {
        bytes32[] memory inheritorHases = new bytes32[](2);
        inheritorHases[0] = keccak256(abi.encode(address(0)));
        inheritorHases[1] = keccak256(abi.encode(address(1)));
        //DeTrustModelRegistry.TrustModel memory m = modelReg.approvedModels(address(impl_01));
        (bytes1 r,,,,uint256 f) = modelReg.approvedModels(address(impl_01));
        erc20.approve(address(modelReg), f);
        address proxy = factory.deployProxyForTrust(
            address(impl_01), 
            address(this),
            inheritorHases, 
            0,
            detrustName
        );

        erc20.transfer(proxy, sendERC20Amount);
        assertEq(erc20.balanceOf(proxy), sendERC20Amount);
        bytes memory _returnData = Address.functionStaticCall(proxy, abi.encodeWithSignature(
            "trustInfo_01()"
        ));
        DeTrustModel_01_Executive.DeTrustModelStorage_01 memory t 
           = abi.decode(_returnData, (DeTrustModel_01_Executive.DeTrustModelStorage_01));
        console2.log(' TRust name %s:; \n LastOwnerOp %s: ', t.name, t.lastOwnerOp);
        
         
        bytes memory payload = abi.encodeWithSignature("transfer(address,uint256)", address(2), sendERC20Amount);
        //vm.prank(msg.sender);
        _returnData = Address.functionCall(proxy, abi.encodeWithSignature(
            "executeOp(address,uint256,bytes)",
            address(erc20), 0, payload
        ));
        assertEq(erc20.balanceOf(address(2)), sendERC20Amount);

        payload = abi.encodeWithSignature("balanceOf(address)", address(2));
        
        // Native EVM Static call
        // (, _returnData) = address(proxy).staticcall(abi.encodeWithSignature(
        //     "staticCallOp(address,bytes)",
        //     address(erc20), payload
        // ));
       
        // OpenZeppelin Static call
        _returnData = Address.functionStaticCall(proxy, abi.encodeWithSignature(
            "staticCallOp(address,bytes)",
            address(erc20), payload
        ));

        // Workaround
        assertEq(
           bytes24(bytes32(erc20.balanceOf(address(2)))), 
           bytes24(_returnData)
        );
        
        // sendEtherAmount = abi.decode(_returnData, (uint256));
        // assertEq(erc20.balanceOf(address(2)), sendEtherAmount);

    }


    function test_execMultiOp() public {
        bytes32[] memory inheritorHases = new bytes32[](2);
        inheritorHases[0] = keccak256(abi.encode(address(0)));
        inheritorHases[1] = keccak256(abi.encode(address(1)));
        //DeTrustModelRegistry.TrustModel memory m = modelReg.approvedModels(address(impl_01));
        (bytes1 r,,,,uint256 f) = modelReg.approvedModels(address(impl_01));
        erc20.approve(address(modelReg), f);
        address proxy = factory.deployProxyForTrust(
            address(impl_01), 
            address(this),
            inheritorHases, 
            0,
            detrustName
        );
        //////////

        erc20.transfer(proxy, sendERC20Amount);
        payable(proxy).transfer(sendEtherAmount);
        assertEq(erc20.balanceOf(proxy), sendERC20Amount);
        assertEq(proxy.balance, sendEtherAmount);
       
        address[] memory _targetArray = new address[](3);
        uint256[] memory _valueArray = new uint256[](3);
        bytes[] memory _dataArray = new bytes[](3);
        _targetArray[0] = address(erc20);
        _targetArray[1] = address(erc20);
        _targetArray[2] = address(this);

        _valueArray[0] = 0;
        _valueArray[1] = 0;
        _valueArray[2] = 7;

        _dataArray[0] = abi.encodeWithSignature("transfer(address,uint256)", address(2), sendERC20Amount / 2);
        _dataArray[1] = abi.encodeWithSignature("approve(address,uint256)", address(this), sendERC20Amount / 2);
        _dataArray[2] = abi.encodeWithSignature("rEth()");
         
        bytes memory _returnData = Address.functionCall(proxy, abi.encodeWithSignature(
            "executeMultiOp(address[],uint256[],bytes[])",
            _targetArray, _valueArray, _dataArray
        ));

        assertEq(erc20.balanceOf(address(2)), sendERC20Amount/2);
        assertEq(erc20.allowance(address(proxy), address(this)), sendERC20Amount/2);
        assertEq(receiveEtherAmount, 7);
    }
}
