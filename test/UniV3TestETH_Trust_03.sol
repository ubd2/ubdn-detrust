// SPDX-License-Identifier: MIT
pragma solidity ^0.8.20;

import "forge-std/Test.sol";
import "forge-std/console.sol";

import {DeTrustFactory} from "../src/DeTrustFactory.sol";
import {DeTrustModel_01_Executive} from "../src/DeTrustModel_01_Executive.sol";
import {DeTrustModelRegistry} from "../src/DeTrustModelRegistry.sol";
import {UsersDeTrustRegistry} from "../src/UsersDeTrustRegistry.sol";


import {ISwapRouter} from'@uniswap/v3-periphery/contracts/interfaces/ISwapRouter.sol';
import '@uniswap/v3-periphery/contracts/interfaces/IQuoter.sol';
import '@uniswap/v3-periphery/contracts/libraries/TransferHelper.sol';
import {IERC20} from "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import {Address} from "@openzeppelin/contracts/utils/Address.sol";


// ETH Mainnet addresses
address constant WETH = 0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2;
address constant DAI = 0x6B175474E89094C44Da98b954EedeAC495271d0F;
address constant USDT = 0xdAC17F958D2ee523a2206206994597C13D831ec7;
address constant WBTC = 0x2260FAC5E5542a773Aa44fBCfeDf7C193bc2C599;

//address constant Factory = 0xB9097ef1c733824d21e52FACe124f65A1a61b984;
//address constant Impl_01 = 0xE691b58fe9ff159e35d68D0783f8044203292782;
address constant SWAP_ROUTER = 0xE592427A0AEce92De3Edee1F18E0157C05861564;
address constant UNI_QUOTER = 0xb27308f9F90D607463bb33eA1BeBb41C27CE5AB6;

// the trust uses swapping in Uniswap v3
contract UniV3TestETH_Trust_03 is Test {

    IERC20 private dai = IERC20(DAI);
    IERC20 private wbtc = IERC20(WBTC);
    //IERC20Mint private usdt = IERC20Mint(USDT);
    IERC20 private usdt = IERC20(USDT);
    uint256 public feeAmount = 5000000;
    uint256 public sendERC20Amount = 100_000_000;
    uint64 public silentPeriod = 10000;
    string public detrustName = 'NameOfDeTrust';
    bytes32[] inheritorHashes = new bytes32[](10);
    uint24 protocolFee = 3000;
    uint160 sqrtPriceLimitX96 = 0;

    DeTrustFactory  public factory;
    DeTrustModel_01_Executive public impl_01;
    UsersDeTrustRegistry public userReg;
    DeTrustModelRegistry public modelReg;
    ISwapRouter public constant router = ISwapRouter(SWAP_ROUTER);

    function setUp() public {
        //DeTrustFactory factory = DeTrustFactory(Factory);
        //DeTrustModel_01_Executive impl_01 = DeTrustModel_01_Executive(Impl_01);
        impl_01 = new DeTrustModel_01_Executive();
        modelReg = new DeTrustModelRegistry(address(100)); //(msg.sender);
        factory = new DeTrustFactory(address(modelReg), address(userReg));
        userReg = new UsersDeTrustRegistry();

        
        modelReg.setModelState(
            address(impl_01),
            DeTrustModelRegistry.TrustModel(0x05, 
                address(0), 
                0, 
                address(USDT), 
                feeAmount)
        );
        userReg.setFactoryState(address(factory), true);
        // prepare inheritor's hashes - 10 inheritors
        //bytes32[] memory inheritorHashes = new bytes32[](10);
        for (uint160 i = 0; i < 10; i++) {
            inheritorHashes[i] =  keccak256(abi.encode(address(i)));
        }

        // swap eth to usdt in Uniswap v3 to work
        ISwapRouter.ExactInputSingleParams memory params = ISwapRouter
            .ExactInputSingleParams({
                tokenIn: WETH,
                tokenOut: USDT, 
                fee: uint24(protocolFee),
                recipient: address(this),
                deadline: block.timestamp,
                amountIn: 1e18,
                amountOutMinimum: 0,
                sqrtPriceLimitX96: sqrtPriceLimitX96
        });

        uint256 amountOut = router.exactInputSingle{value: 1e18}(params);
    }

    function test_Proxy() public {
        // deploy trust
        TransferHelper.safeApprove(USDT, address(modelReg), feeAmount);
        uint256 balanceBeforeBen = usdt.balanceOf(address(100)); // fee beneficiary balance
        address proxy = factory.deployProxyForTrust(
            address(impl_01), 
            address(11),  // _creator
            inheritorHashes, // _inheritorHashes
            silentPeriod,    //_silence
            detrustName     //_name
        );

        // topup trust
        TransferHelper.safeTransfer(USDT, proxy, sendERC20Amount);
        assertEq(usdt.balanceOf(proxy), sendERC20Amount);

        address[] memory _targetArray = new address[](2);
        uint256[] memory _valueArray = new uint256[](2);
        bytes[] memory _dataArray = new bytes[](2);

        _targetArray[0] = USDT;
        _targetArray[1] = SWAP_ROUTER;

        _valueArray[0] = 0;
        _valueArray[1] = 0;


        _dataArray[0] = abi.encodeWithSignature("approve(address,uint256)", SWAP_ROUTER, sendERC20Amount);

        ISwapRouter.ExactInputSingleParams memory params = ISwapRouter
            .ExactInputSingleParams({
                tokenIn: USDT,
                tokenOut: DAI, 
                fee: uint24(protocolFee),
                recipient: address(11),
                deadline: block.timestamp,
                amountIn: sendERC20Amount,
                amountOutMinimum: 0,
                sqrtPriceLimitX96: sqrtPriceLimitX96
        });

        _dataArray[1] = abi.encodeWithSignature("exactInputSingle((address,address,uint24,address,uint256,uint256,uint256,uint160))", params);
        
        uint256 amountOut = IQuoter(UNI_QUOTER).quoteExactInputSingle(
                USDT,
                DAI,
                protocolFee,
                sendERC20Amount,
                0
            );

        vm.startPrank(address(11));

        bytes memory _returnData = Address.functionCall(proxy, abi.encodeWithSignature(
            "executeMultiOp(address[],uint256[],bytes[])",
            _targetArray, _valueArray, _dataArray
        ));

        assertEq(usdt.balanceOf(proxy), 0);
        assertEq(dai.balanceOf(address(11)), amountOut);
    
    }
}