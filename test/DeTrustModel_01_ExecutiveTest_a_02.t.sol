// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.13;

import {Test, console2} from "forge-std/Test.sol";
import {Address} from "@openzeppelin/contracts/utils/Address.sol";
import "forge-std/console.sol";

import {DeTrustFactory} from "../src/DeTrustFactory.sol";
import {DeTrustModel_01_Executive} from "../src/DeTrustModel_01_Executive.sol";
import {MockERC20} from "../src/mock/MockERC20.sol";

import {UsersDeTrustRegistry} from "../src/UsersDeTrustRegistry.sol";
import {DeTrustModelRegistry} from "../src/DeTrustModelRegistry.sol";

import {ITrustModel_00} from "../src/interfaces/ITrustModel_00.sol";

contract DeTrustModel_01_ExecutiveTest_a_02 is Test {
    uint256 public sendEtherAmount = 1e18;
    uint256 public sendERC20Amount = 2e18;
    uint256 public feeAmount = 5e18;
    uint64 public silentPeriod = 10000;
    string public detrustName = 'NameOfDeTrust';
    string public badDetrustName = 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA';
    error AddressInsufficientBalance(address account);

    DeTrustFactory  public factory;
    DeTrustModel_01_Executive public impl_01;
    UsersDeTrustRegistry public userReg;
    DeTrustModelRegistry public modelReg;
    bytes32[] inheritorHashes = new bytes32[](10);

    MockERC20 public erc20;

    receive() external payable virtual {}
    function setUp() public {
        impl_01 = new DeTrustModel_01_Executive();
        erc20 = new MockERC20('UBDN token', 'UBDN');
        modelReg = new DeTrustModelRegistry(address(100)); //(msg.sender);
        userReg = new UsersDeTrustRegistry();
        factory = new DeTrustFactory(address(modelReg), address(userReg));

        // with fee to create trust
        modelReg.setModelState(
            address(impl_01),
            DeTrustModelRegistry.TrustModel(0x05, address(0), 0, address(erc20), feeAmount)
        );
        console.logBytes1(modelReg.isModelEnable(address(impl_01), address(0)));

        userReg.setFactoryState(address(factory), true);
        assertEq(
            uint8(modelReg.isModelEnable(address(impl_01), address(0))), 
            uint8(0x05)
        );
        // prepare inheritor's hashes - 10 inheritors
        //bytes32[] memory inheritorHashes = new bytes32[](10);
        for (uint160 i = 0; i < 10; i++) {
            inheritorHashes[i] =  keccak256(abi.encode(address(i)));
        }
    }

    function test_proxy_annual_fee_one_period() public {
        assertEq(address(factory.modelRegistry()), address(modelReg));
        assertEq(address(factory.trustRegistry()), address(userReg));

        // add balance for msg.sender
        erc20.transfer(address(11), feeAmount);
        vm.startPrank(address(11));
        erc20.approve(address(modelReg), feeAmount);
        uint256 balanceBeforeBen = erc20.balanceOf(address(100)); // fee beneficiary balance
        address proxy = factory.deployProxyForTrust(
            address(impl_01), 
            address(11),  // _creator
            inheritorHashes, // _inheritorHashes
            silentPeriod,    //_silence
            detrustName     //_name
        );
        vm.stopPrank();
        // check fee payment
        assertEq(erc20.balanceOf(address(11)), 0);
        assertEq(erc20.balanceOf(address(100)), balanceBeforeBen + feeAmount);


        // get proxy info
        bytes memory _data = abi.encodeWithSelector(DeTrustModel_01_Executive.trustInfo_01.selector);
        bytes memory _returnData = Address.functionStaticCall(proxy, _data);
        DeTrustModel_01_Executive.DeTrustModelStorage_01 memory proxyInfo = abi.decode(
            _returnData, 
            (DeTrustModel_01_Executive.DeTrustModelStorage_01));
        assertEq(proxyInfo.lastOwnerOp, block.timestamp);
        assertEq(proxyInfo.inherited, false);
        assertEq(proxyInfo.creator, address(11));
        assertEq(proxyInfo.name, detrustName);
        assertEq(proxyInfo.silenceTime, silentPeriod);
        assertEq(proxyInfo.inheritorHashes[9], inheritorHashes[9]);
        assertEq(proxyInfo.fee.feeToken, address(erc20));
        assertEq(proxyInfo.fee.feeAmount, feeAmount);
        assertEq(proxyInfo.fee.feeBeneficiary, modelReg.feeBeneficiary());
        
        _returnData = Address.functionStaticCall(proxy, abi.encodeWithSignature(
            "ANNUAL_FEE_PERIOD()"
        ));
        uint256 annual_fee_period = uint256(bytes32(_returnData));
        assertEq(proxyInfo.fee.payedTill, uint64(block.timestamp) + annual_fee_period);

        
        // topup trust
        erc20.transfer(proxy, sendERC20Amount);
        address payable _receiver = payable(proxy);
        _receiver.transfer(sendEtherAmount);

        // move time - creator doesn't pay fee because payment date doesn't come 
        vm.warp(block.timestamp + 100);
        // creator withdraws ether in silent period
        vm.prank(address(11));
        _returnData = Address.functionCall(proxy, abi.encodeWithSignature(
            "transferNative(address,uint256)",
            address(1), sendEtherAmount / 4  
        ));
        assertEq(address(1).balance, sendEtherAmount / 4); // check recipient eth balance

        // get proxy info
        _data = abi.encodeWithSelector(DeTrustModel_01_Executive.trustInfo_01.selector);
        _returnData = Address.functionStaticCall(proxy, _data);
        DeTrustModel_01_Executive.DeTrustModelStorage_01 memory proxyInfo1 = abi.decode(
            _returnData, 
            (DeTrustModel_01_Executive.DeTrustModelStorage_01));
        assertEq(proxyInfo1.lastOwnerOp, block.timestamp);
        assertEq(proxyInfo1.inherited, false);
        assertEq(proxyInfo.fee.payedTill, proxyInfo1.fee.payedTill); // nothing has changed

        // move time - creator should pay fee because annual payment date has come 
        // inheritors can get money from the trust
        vm.warp(block.timestamp + annual_fee_period);
        vm.prank(address(1));
        vm.expectRevert(); // there is not enough msg.sender's balance to pay fee
        _returnData = Address.functionCall(proxy, abi.encodeWithSignature(
            "transferNative(address,uint256)",
            address(1), sendEtherAmount / 4  
        ));
        assertEq(address(1).balance, sendEtherAmount / 4); // check recipient eth balance

        erc20.transfer(address(1), feeAmount);
        balanceBeforeBen = erc20.balanceOf(address(100)); // fee beneficiary balance
        vm.startPrank(address(1));
        erc20.transfer(address(proxy), feeAmount); //topup proxy to pay fee
        uint256 balanceBeforeProxy = erc20.balanceOf(address(proxy)); // proxy balance
        _returnData = Address.functionCall(proxy, abi.encodeWithSignature(
            "transferNative(address,uint256)",
            address(1), sendEtherAmount / 4  
        ));
        vm.stopPrank();
        assertEq(address(1).balance, sendEtherAmount / 2);
        // check fee payment
        assertEq(erc20.balanceOf(address(proxy)), balanceBeforeProxy - feeAmount);
        assertEq(erc20.balanceOf(address(100)), balanceBeforeBen + feeAmount);

        // get proxy info
        _data = abi.encodeWithSelector(DeTrustModel_01_Executive.trustInfo_01.selector);
        _returnData = Address.functionStaticCall(proxy, _data);
        DeTrustModel_01_Executive.DeTrustModelStorage_01 memory proxyInfo2 = abi.decode(
            _returnData, 
            (DeTrustModel_01_Executive.DeTrustModelStorage_01));
        assertEq(proxyInfo2.fee.payedTill, proxyInfo1.fee.payedTill + annual_fee_period);  //check payedTill date of the trust
        
        erc20.transfer(address(1), feeAmount * 3);

        // move time to pay several fee payments
        vm.warp(block.timestamp + (annual_fee_period * 3));
        vm.startPrank(address(1));
        erc20.transfer(address(proxy), feeAmount * 3); // topup proxy to pay fee
        balanceBeforeBen = erc20.balanceOf(address(100)); // fee beneficiary balance
        balanceBeforeProxy = erc20.balanceOf(address(proxy)); // proxy balance
        _returnData = Address.functionCall(proxy, abi.encodeWithSignature(
            "transferERC20(address,address,uint256)",
            address(erc20),address(1), sendERC20Amount / 2  
        ));
        vm.stopPrank();
        assertEq(erc20.balanceOf(address(1)), sendERC20Amount / 2);
        // check fee payment
        assertEq(erc20.balanceOf(proxy), balanceBeforeProxy - feeAmount * 3 - sendERC20Amount / 2);
        assertEq(erc20.balanceOf(address(100)), balanceBeforeBen + feeAmount * 3);

        // get proxy info
        _data = abi.encodeWithSelector(DeTrustModel_01_Executive.trustInfo_01.selector);
        _returnData = Address.functionStaticCall(proxy, _data);
        DeTrustModel_01_Executive.DeTrustModelStorage_01 memory proxyInfo3 = abi.decode(
            _returnData, 
            (DeTrustModel_01_Executive.DeTrustModelStorage_01));
        assertEq(proxyInfo3.fee.payedTill, proxyInfo2.fee.payedTill + annual_fee_period * 3); //check payedTill date of the trust
    }
}
