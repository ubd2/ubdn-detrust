// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.13;

import {Test, console2} from "forge-std/Test.sol";
import {Address} from "@openzeppelin/contracts/utils/Address.sol";
import "forge-std/console.sol";

import {DeTrustFactory} from "../src/DeTrustFactory.sol";
import {DeTrustModel_00} from "../src/DeTrustModel_00.sol";
import {MockERC20} from "../src/mock/MockERC20.sol";

import {UsersDeTrustRegistry} from "../src/UsersDeTrustRegistry.sol";
import {DeTrustModelRegistry} from "../src/DeTrustModelRegistry.sol";

import {ITrustModel_00} from "../src/interfaces/ITrustModel_00.sol";

contract FactoryTest_a_02 is Test {
    uint256 public sendEtherAmount = 1e18;
    uint256 public sendERC20Amount = 2e18;
    uint256 public neededERC20Amount = 5e18;
    uint64 public silentPeriod = 10000;
    string public detrustName = 'NameOfDeTrust';
    string public badDetrustName = 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA';
    error AddressInsufficientBalance(address account);

    DeTrustFactory  public factory;
    DeTrustModel_00 public impl_00;
    DeTrustModel_00 public impl_01;
    DeTrustModel_00 public impl_02;
    UsersDeTrustRegistry public userReg;
    DeTrustModelRegistry public modelReg;

    MockERC20 public erc20;

    receive() external payable virtual {}
    function setUp() public {
        impl_00 = new DeTrustModel_00();
        impl_01 = new DeTrustModel_00();
        impl_02 = new DeTrustModel_00();
        erc20 = new MockERC20('UBDN token', 'UBDN');
        modelReg = new DeTrustModelRegistry(msg.sender);
        userReg = new UsersDeTrustRegistry();
        factory = new DeTrustFactory(address(modelReg), address(userReg));

        userReg.setFactoryState(address(factory), true);
    }

    function test_registry() public {

        // without any requirements
        modelReg.setModelState(
            address(impl_00),
            DeTrustModelRegistry.TrustModel(0x01, address(0), 0, address(0), 0)
        );
        console.logBytes1(modelReg.isModelEnable(address(impl_00), address(0)));

        // send transaction from address(1) - balance is zero, expect revert
        vm.prank(address(1));
        address proxy = factory.deployProxyForTrust(
            address(impl_00), 
            msg.sender,
            keccak256(abi.encode(address(2))), 
            silentPeriod,
            detrustName
        );

        // registry with checking of balance
        modelReg.setModelState(
            address(impl_01),
            DeTrustModelRegistry.TrustModel(0x03, address(erc20), 1, address(0), 0)
        );

        vm.prank(address(1));
        vm.expectRevert("Too low Balance");
        proxy = factory.deployProxyForTrust(
            address(impl_01), 
            msg.sender,
            keccak256(abi.encode(address(2))), 
            silentPeriod,
            detrustName
        );

        // registry with fee
        vm.expectRevert("Please enable check balance");
        modelReg.setModelState(
            address(impl_01),
            DeTrustModelRegistry.TrustModel(0x05, address(erc20), 1, address(0), 0)
        );

        modelReg.setModelState(
            address(impl_02),
            DeTrustModelRegistry.TrustModel(0x05, address(0), 0, address(0), 0)
        );

        // remove model
        modelReg.removeModel(address(impl_01));
        assertEq(modelReg.modelsList(0), address(impl_00));
        assertEq(modelReg.modelsList(1), address(impl_02));
        assertEq(
            uint8(modelReg.isModelEnable(address(impl_01), address(this))),
            uint8(0x00)
        ); // model is not registred
       console.log(vm.toString(keccak256(abi.encode(0xf315B9006C20913D6D8498BDf657E778d4Ddf2c4))));
    }

}