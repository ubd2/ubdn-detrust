// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.13;

import {Test, console2} from "forge-std/Test.sol";
import {Address} from "@openzeppelin/contracts/utils/Address.sol";

import {DeTrustFactory} from "../src/DeTrustFactory.sol";
import {DeTrustModel_00} from "../src/DeTrustModel_00.sol";
import {MockERC20} from "../src/mock/MockERC20.sol";
//import {ITrustModel_00} from "../src/interfaces/ITrustModel_00.sol";

contract FactoryTest_m_01 is Test {
    uint256 public sendEtherAmount = 1e18;
    uint256 public sendERC20Amount = 2e18;
    string public detrustName = 'NameOfDeTrust';
    DeTrustFactory  public factory;
    DeTrustModel_00 public impl_00;
    //eTrustModel_00 public payable impl_00_instance;


    MockERC20 public erc20;

    receive() external payable virtual {}
    function setUp() public {
        factory = new DeTrustFactory(address(0), address(0));
        impl_00 = new DeTrustModel_00();
        erc20 = new MockERC20('Mock ERC20 Token', 'MOCK');
    }

    function test_zeroRegistry() public {
        assertEq(address(factory.modelRegistry()), address(0));
        assertEq(address(factory.trustRegistry()), address(0));
    }

    function test_proxyAddress() public {
        address proxy = factory.deployProxyForTrust(
            address(impl_00), 
            msg.sender,
            keccak256(abi.encode(address(0))), 
            0,
            detrustName
        );
        console2.log("Implementation   addr: %s", address(impl_00));
        console2.log("Proxy for     impl_00: %s", proxy);
        assertFalse(address(impl_00) == proxy);
        //assertEq(address(impl_00), proxy);
    }


    function test_erc20Balance() public {

        address proxy = factory.deployProxyForTrust(
            address(impl_00), 
            msg.sender,
            keccak256(abi.encode(address(0))), 
            0,
            detrustName
        );

        //DeTrustModel_00 impl_00_instance = DeTrustModel_00(proxy);
        //DeTrustModel_00.DeTrustModelStorage_00 memory s = impl_00_instance.trustInfo();
        
        bytes memory _data = abi.encodeWithSelector(DeTrustModel_00.trustInfo.selector);
        bytes memory returndata = Address.functionStaticCall(proxy, _data);
        DeTrustModel_00.DeTrustModelStorage memory s = abi.decode(returndata, (DeTrustModel_00.DeTrustModelStorage));
        
        console2.log("Trust creator: %s", s.creator);
        console2.log("Last op: %s", s.lastOwnerOp);
        console2.log("ST: %s", s.silenceTime);
        console2.log("inherited: %s", s.inherited);

        assertEq(erc20.balanceOf(proxy), 0);
        assertEq(erc20.balanceOf(address(this)), erc20.totalSupply());
        erc20.transfer(proxy, sendERC20Amount);
        assertEq(erc20.balanceOf(proxy), sendERC20Amount);
        vm.prank(msg.sender);
        bytes memory _returnData = Address.functionCall(proxy, abi.encodeWithSignature(
            "transferERC20(address,address,uint256)",
            address(erc20), address(this), sendERC20Amount/2
        ));
        assertEq(erc20.balanceOf(proxy), sendERC20Amount/2);

    }

    function test_etherBalance() public {

        address proxy = factory.deployProxyForTrust(
            address(impl_00), 
            msg.sender,
            keccak256(abi.encode(address(0))), 
            0,
            detrustName
        );
        console2.log("Implementation   addr: %s", address(impl_00));
        console2.log("Proxy for     impl_00: %s", proxy);
        console2.log("Msg.sender in    test: %s", msg.sender);
        console2.log("Test contract address: %s", msg.sender);
        console2.log("ETH balnce of Test contract: %s", address(this).balance);
        assertEq(proxy.balance, 0);
        assertFalse(address(this).balance == 0);
        address payable _receiver = payable(proxy);
        _receiver.transfer(sendEtherAmount);
        assertEq(address(proxy).balance, sendEtherAmount);
        bytes memory _returnData = Address.functionStaticCall(proxy, abi.encodeWithSignature(
            "creator()"
        ));
        address _creatorFromCall = address(uint160(uint256(bytes32(_returnData))));
        console2.log("Creator from staticcal: %s", _creatorFromCall);
        console2.log("ETH balnce of Test contract: %s", address(this).balance);
        vm.prank(msg.sender);
        _returnData = Address.functionCall(proxy, abi.encodeWithSignature(
            "transferNative(address,uint256)",
            _creatorFromCall, sendEtherAmount/2
            
        ));
        assertEq(address(proxy).balance, sendEtherAmount/2);

    }
}
