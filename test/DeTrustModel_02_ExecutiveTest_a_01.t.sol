// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.13;

import {Test, console2} from "forge-std/Test.sol";
import {Address} from "@openzeppelin/contracts/utils/Address.sol";
import "forge-std/console.sol";

import {DeTrustFactory} from "../src/DeTrustFactory.sol";
import {DeTrustModel_02_Executive} from "../src/DeTrustModel_02_Executive.sol";
import {MockERC20} from "../src/mock/MockERC20.sol";

import {UsersDeTrustRegistry} from "../src/UsersDeTrustRegistry.sol";
import {DeTrustModelRegistry} from "../src/DeTrustModelRegistry.sol";

import {ITrustModel_00} from "../src/interfaces/ITrustModel_00.sol";

contract DeTrustModel_02_ExecutiveTest_a_01 is Test {
    uint256 public sendEtherAmount = 1e18;
    uint256 public sendERC20Amount = 2e18;
    uint256 public feeAmount = 5e18;
    uint64 public inheritedTime = 10000;
    string public detrustName = 'NameOfDeTrust';
    string public badDetrustName = 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA';
    error AddressInsufficientBalance(address account);

    DeTrustFactory  public factory;
    DeTrustModel_02_Executive public impl_02;
    UsersDeTrustRegistry public userReg;
    DeTrustModelRegistry public modelReg;
    bytes32[] inheritorHashes = new bytes32[](10);

    MockERC20 public erc20;

    receive() external payable virtual {}
    function setUp() public {
        impl_02 = new DeTrustModel_02_Executive();
        erc20 = new MockERC20('UBDN token', 'UBDN');
        modelReg = new DeTrustModelRegistry(address(100)); //(msg.sender);
        userReg = new UsersDeTrustRegistry();
        factory = new DeTrustFactory(address(modelReg), address(userReg));

        // with fee to create trust
        modelReg.setModelState(
            address(impl_02),
            DeTrustModelRegistry.TrustModel(0x05, address(0), 0, address(erc20), feeAmount)
        );
        console.logBytes1(modelReg.isModelEnable(address(impl_02), address(0)));

        userReg.setFactoryState(address(factory), true);
        assertEq(
            uint8(modelReg.isModelEnable(address(impl_02), address(0))), 
            uint8(0x05)
        );
        // prepare inheritor's hashes - 10 inheritors
        //bytes32[] memory inheritorHashes = new bytes32[](10);
        for (uint160 i = 0; i < 10; i++) {
            inheritorHashes[i] =  keccak256(abi.encode(address(i)));
        }
    }

    function test_proxy() public {
        assertEq(address(factory.modelRegistry()), address(modelReg));
        assertEq(address(factory.trustRegistry()), address(userReg));

        // add balance for msg.sender
        erc20.transfer(address(11), feeAmount);
        vm.startPrank(address(11));
        erc20.approve(address(modelReg), feeAmount);
        uint256 balanceBefore = erc20.balanceOf(address(100)); // fee beneficiary balance
        address proxy = factory.deployProxyForTrust(
            address(impl_02), 
            address(11),  // _creator
            inheritorHashes, // _inheritorHashes
            inheritedTime,    // _inheritDate
            detrustName     //_name
        );
        vm.stopPrank();
        // check fee payment
        assertEq(erc20.balanceOf(address(11)), 0);
        assertEq(erc20.balanceOf(address(100)), balanceBefore + feeAmount);

        // get proxy info
        bytes memory _data = abi.encodeWithSelector(DeTrustModel_02_Executive.trustInfo_02.selector);
        bytes memory _returnData = Address.functionStaticCall(proxy, _data);
        DeTrustModel_02_Executive.DeTrustModelStorage_02 memory proxyInfo = abi.decode(
            _returnData, 
            (DeTrustModel_02_Executive.DeTrustModelStorage_02));
        assertEq(proxyInfo.creator, address(11));
        assertEq(proxyInfo.name, detrustName);
        assertEq(proxyInfo.inheritedDate, inheritedTime);
        assertEq(proxyInfo.inheritorHashes[9], inheritorHashes[9]);
        assertEq(proxyInfo.fee.feeToken, address(erc20));
        assertEq(proxyInfo.fee.feeAmount, feeAmount);
        assertEq(proxyInfo.fee.feeBeneficiary, modelReg.feeBeneficiary());

        // check UsersDeTrustRegistry
        assertEq(userReg.getInheritorTrusts(address(1))[0], proxy);
        assertEq(userReg.getInheritorTrusts(address(9))[0], proxy);

        
        _returnData = Address.functionStaticCall(proxy, abi.encodeWithSignature(
            "ANNUAL_FEE_PERIOD()"
        ));
        uint256 annual_fee_period = uint256(bytes32(_returnData));
        assertEq(proxyInfo.fee.payedTill, uint64(block.timestamp) + annual_fee_period);

        
        // topup trust
        erc20.transfer(proxy, sendERC20Amount);
        address payable _receiver = payable(proxy);
        _receiver.transfer(sendEtherAmount);

        // move time
        vm.warp(block.timestamp + 100);
        // creator withdraws ether in silent period
        vm.prank(address(11));
        _returnData = Address.functionCall(proxy, abi.encodeWithSignature(
            "transferNative(address,uint256)",
            address(1), sendEtherAmount / 4  
        ));

        // an outsider tries to withdraw in the silent period (caller not in inheritors list)
        vm.prank(address(10));
        vm.expectRevert(
            abi.encodeWithSelector(DeTrustModel_02_Executive.OwnableUnauthorizedAccount.selector, address(10))
        );
        _returnData = Address.functionCall(proxy, abi.encodeWithSignature(
            "transferNative(address,uint256)",
            address(4), sendEtherAmount / 2
            
        ));

        // one of the inheriters tries to withdraw ether in the silent period
        console.log("Ether inheriter balance before : %s", address(2).balance);
        
        vm.prank(address(2));
        vm.expectRevert(
            abi.encodeWithSelector(DeTrustModel_02_Executive.OwnableUnauthorizedAccount.selector, address(2))
        );
        _returnData = Address.functionCall(proxy, abi.encodeWithSignature(
            "transferNative(address,uint256)",
            address(4), sendEtherAmount / 2 
        ));
        assertEq(address(4).balance, 0);

        console.log("Ether inheriter balance after withdraw attempt: %s", address(4).balance);
        console.log("Current block time: %s", block.timestamp);
        
        // move time //
        // the silent period is ended
        vm.warp(block.timestamp + inheritedTime + 1);
        console.log("Block time after the moving: %s", block.timestamp);

        // an inheriter tries to withdraw again ether
        vm.startPrank(address(2));
        
        // check emit
        vm.expectEmit();
        emit DeTrustModel_02_Executive.EtherTransfer(sendEtherAmount / 2);
        _returnData = Address.functionCall(proxy, abi.encodeWithSignature(
            "transferNative(address,uint256)",
            address(4), sendEtherAmount / 2 
        ));

        assertEq(address(4).balance, sendEtherAmount / 2 );

        console.log("Ether inheriter balance after withdraw attempt: %s", address(4).balance);

        // For DEBUG only !!!!!!, Inheritor call MUST NOT change lastOwnerOp
        // move time one more time )). Because previose call set timestamp.
        // the silent period is ended
        //vm.warp(block.timestamp + silentPeriod + 1);
        

        // an inheriter withdraws erc20 tokens
        //vm.prank(address(2));
        _returnData = Address.functionCall(proxy, abi.encodeWithSignature(
            "transferERC20(address,address,uint256)",
             address(erc20), address(4), sendERC20Amount / 4 
        ));
        vm.stopPrank();
        assertEq(erc20.balanceOf(address(4)), sendERC20Amount / 4 );     
        assertEq(erc20.balanceOf(proxy), sendERC20Amount * 3 / 4 );  

        // silent period is ended.
        // inheriter entered into the rights
        // creator withdraw money from the trust 
        // check recipient balance before
        assertEq(erc20.balanceOf(address(5)), 0);
        vm.prank(address(11)); 
        _returnData = Address.functionCall(proxy, abi.encodeWithSignature(
            "transferERC20(address,address,uint256)",
             address(erc20), address(5), sendERC20Amount / 4 
        ));
        // check recipient balance after
        assertEq(erc20.balanceOf(address(5)), sendERC20Amount / 4);

        // outsider tries to withdraw money from the trust after silent period
        vm.prank(address(12)); 
        vm.expectRevert(
            abi.encodeWithSelector(DeTrustModel_02_Executive.OwnableUnauthorizedAccount.selector, address(12))
        );
        _returnData = Address.functionCall(proxy, abi.encodeWithSignature(
            "transferERC20(address,address,uint256)",
             address(erc20), address(12), sendERC20Amount / 4 
        ));
        // check recipient balance after
        assertEq(erc20.balanceOf(address(12)), 0);

        vm.startPrank(address(11)); 
        // try to withdraw ethers more than proxy has
        vm.expectRevert(
            abi.encodeWithSelector(AddressInsufficientBalance.selector, address(proxy))
        );
        _returnData = Address.functionCall(proxy, abi.encodeWithSignature(
            "transferNative(address,uint256)",
            address(4), sendEtherAmount * 10 
        ));

    }
}