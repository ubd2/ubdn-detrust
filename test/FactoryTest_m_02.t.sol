// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.13;

import {Test, console2} from "forge-std/Test.sol";
import {Address} from "@openzeppelin/contracts/utils/Address.sol";

import {DeTrustFactory} from "../src/DeTrustFactory.sol";
import {DeTrustModel_00} from "../src/DeTrustModel_00.sol";
import {MockERC20} from "../src/mock/MockERC20.sol";

import {UsersDeTrustRegistry} from "../src/UsersDeTrustRegistry.sol";
import {DeTrustModelRegistry} from "../src/DeTrustModelRegistry.sol";


contract FactoryTest_m_02 is Test {
    uint256 public sendEtherAmount = 1e18;
    uint256 public sendERC20Amount = 2e18;
    string public detrustName = 'NameOfDeTrust';
    DeTrustFactory  public factory;
    DeTrustModel_00 public impl_00;
    UsersDeTrustRegistry public userReg;
    DeTrustModelRegistry public modelReg;

    MockERC20 public erc20;

    receive() external payable virtual {}
    function setUp() public {
        modelReg = new DeTrustModelRegistry(msg.sender);
        userReg = new UsersDeTrustRegistry();
        factory = new DeTrustFactory(address(modelReg), address(userReg));
        impl_00 = new DeTrustModel_00();
        erc20 = new MockERC20('Mock ERC20 Token', 'MOCK');
    }

    function test_nonZeroRegistry() public {
        assertFalse(address(factory.modelRegistry()) == address(0));
        assertFalse(address(factory.trustRegistry()) == address(0));
    }

    function test_createProxyFail() public {
        vm.expectRevert("Model not approved");
        address proxy = factory.deployProxyForTrust(
            address(impl_00), 
            msg.sender,
            keccak256(abi.encode(address(0))), 
            0,
            detrustName
        );
        modelReg.setModelState(
            address(impl_00),
            DeTrustModelRegistry.TrustModel(0x01, address(0), 0, address(0), 0)
        );
         
        vm.expectRevert("NonAuthorized factory");
        proxy = factory.deployProxyForTrust(
            address(impl_00), 
            msg.sender,
            keccak256(abi.encode(address(0))), 
            0,
            detrustName
        );

        userReg.setFactoryState(address(factory), true);
        modelReg.setModelState(
            address(impl_00),
            DeTrustModelRegistry.TrustModel(0x03, address(erc20), 1e32, address(0), 0)
        );
        vm.expectRevert("Too low Balance");
        proxy = factory.deployProxyForTrust(
            address(impl_00), 
            msg.sender,
            keccak256(abi.encode(address(0))), 
            0,
            detrustName
        );

        modelReg.setModelState(
            address(impl_00),
            DeTrustModelRegistry.TrustModel(0x03, address(erc20), 100e18, address(0), 0)
        );
        proxy = factory.deployProxyForTrust(
            address(impl_00), 
            msg.sender,
            keccak256(abi.encode(address(0))), 
            0,
            detrustName
        );
        //assertEq(address(impl_00), proxy);
    }

    
    

}
