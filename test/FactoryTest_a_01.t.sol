// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.13;

import {Test, console2} from "forge-std/Test.sol";
import {Address} from "@openzeppelin/contracts/utils/Address.sol";
import "forge-std/console.sol";

import {DeTrustFactory} from "../src/DeTrustFactory.sol";
import {DeTrustModel_00} from "../src/DeTrustModel_00.sol";
import {MockERC20} from "../src/mock/MockERC20.sol";

import {UsersDeTrustRegistry} from "../src/UsersDeTrustRegistry.sol";
import {DeTrustModelRegistry} from "../src/DeTrustModelRegistry.sol";

import {ITrustModel_00} from "../src/interfaces/ITrustModel_00.sol";

contract FactoryTest_a_01 is Test {
    uint256 public sendEtherAmount = 1e18;
    uint256 public sendERC20Amount = 2e18;
    uint256 public neededERC20Amount = 5e18;
    uint64 public silentPeriod = 10000;
    string public detrustName = 'NameOfDeTrust';
    string public badDetrustName = 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA';
    error AddressInsufficientBalance(address account);

    DeTrustFactory  public factory;
    DeTrustModel_00 public impl_00;
    UsersDeTrustRegistry public userReg;
    DeTrustModelRegistry public modelReg;

    MockERC20 public erc20;

    receive() external payable virtual {}
    function setUp() public {
        impl_00 = new DeTrustModel_00();
        erc20 = new MockERC20('UBDN token', 'UBDN');
        modelReg = new DeTrustModelRegistry(msg.sender);
        userReg = new UsersDeTrustRegistry();
        factory = new DeTrustFactory(address(modelReg), address(userReg));

        modelReg.setModelState(
            address(impl_00),
            DeTrustModelRegistry.TrustModel(0x03, address(erc20), neededERC20Amount, address(0), 0)
        );
        console.logBytes1(modelReg.isModelEnable(address(impl_00), address(0)));

        userReg.setFactoryState(address(factory), true);
        assertEq(
            uint8(modelReg.isModelEnable(address(impl_00), address(0))), 
            uint8(0x03)
        );

    }

    function test_proxy() public {
        assertEq(address(factory.modelRegistry()), address(modelReg));
        assertEq(address(factory.trustRegistry()), address(userReg));

        // send transaction from address(1) - balance is zero, expect revert
        vm.prank(address(1));
        vm.expectRevert("Too low Balance");
        address proxy = factory.deployProxyForTrust(
            address(impl_00), 
            msg.sender,
            keccak256(abi.encode(address(2))), 
            silentPeriod,
            detrustName
        );

        // add balance for msg.sender
        erc20.transfer(address(1), neededERC20Amount);

        // use too long name - expect revert
        vm.prank(address(1));
        vm.expectRevert("Too long name");
        proxy = factory.deployProxyForTrust(
            address(impl_00), 
            msg.sender,
            keccak256(abi.encode(address(2))), 
            silentPeriod,
            badDetrustName
        );

        vm.deal(address(1), 100 ether); 
        vm.startPrank(address(1));
        proxy = factory.deployProxyForTrust(
            address(impl_00), 
            address(1), //msg.sender,
            keccak256(abi.encode(address(2))), // inheriter
            silentPeriod,
            detrustName
        );

        // get proxy info
        ITrustModel_00.DeTrustModelStorage_00 memory proxyInfo = ITrustModel_00(proxy).trustInfo();
        assertEq(proxyInfo.lastOwnerOp, block.timestamp);
        assertEq(proxyInfo.inherited, false);
        assertEq(proxyInfo.name, detrustName);
        assertEq(proxyInfo.silenceTime, silentPeriod);
        assertEq(proxyInfo.inheritorHash, keccak256(abi.encode(address(2))));
        assertEq(ITrustModel_00(proxy).creator(), address(1));

        // topup trust
        erc20.transfer(proxy, sendERC20Amount);
        address payable _receiver = payable(proxy);
        _receiver.transfer(sendEtherAmount);

        // move time
        vm.warp(block.timestamp + 100);
        // creator withdraws ether in silent period
        bytes memory _returnData = Address.functionCall(proxy, abi.encodeWithSignature(
            "transferNative(address,uint256)",
            address(1), sendEtherAmount / 4  
        ));

        vm.stopPrank();

        // get proxy info
        proxyInfo = ITrustModel_00(proxy).trustInfo();
        assertEq(proxyInfo.lastOwnerOp, block.timestamp);
        assertEq(proxyInfo.inherited, false);

        // an outsider tries to withdraw in the silent period
        vm.prank(address(3));
        vm.expectRevert(
            abi.encodeWithSelector(DeTrustModel_00.OwnableUnauthorizedAccount.selector, address(3))
        );
        _returnData = Address.functionCall(proxy, abi.encodeWithSignature(
            "transferNative(address,uint256)",
            address(4), sendEtherAmount / 2
            
        ));

        // an inheriter tries to withdraw ether in the silent period
        console.log("Ether inheriter balance before : %s", address(2).balance);
        
        vm.prank(address(2));
        vm.expectRevert(
            abi.encodeWithSelector(DeTrustModel_00.OwnableUnauthorizedAccount.selector, address(2))
        );
        _returnData = Address.functionCall(proxy, abi.encodeWithSignature(
            "transferNative(address,uint256)",
            address(4), sendEtherAmount / 2 
        ));

        console.log("Ether inheriter balance after withdraw attempt: %s", address(2).balance);

        console.log("Current block time: %s", block.timestamp);
        
        // move time //
        // the silent period is ended
        vm.warp(block.timestamp + silentPeriod + 1);
        console.log("Block time after the moving: %s", block.timestamp);

        // an inheriter tries to withdraw again ether
        assertEq(address(4).balance, 0);
        vm.startPrank(address(2));
        
        // check emit
        vm.expectEmit();
        emit DeTrustModel_00.EtherTransfer(sendEtherAmount / 2);
        _returnData = Address.functionCall(proxy, abi.encodeWithSignature(
            "transferNative(address,uint256)",
            address(4), sendEtherAmount / 2 
        ));
        ITrustModel_00.DeTrustModelStorage_00 memory inheritedProxyInfo = ITrustModel_00(proxy).trustInfo();
        assertEq(inheritedProxyInfo.lastOwnerOp, proxyInfo.lastOwnerOp);
        assertEq(inheritedProxyInfo.inherited, true);

        assertEq(address(4).balance, sendEtherAmount / 2 );

        console.log("Ether inheriter balance after withdraw attempt: %s", address(2).balance);

        // For DEBUG only !!!!!!, Inheritor call MUST NOT change lastOwnerOp
        // move time one more time )). Because previose call set timestamp.
        // the silent period is ended
        //vm.warp(block.timestamp + silentPeriod + 1);
        

        // an inheriter withdraws erc20 tokens
        //vm.prank(address(2));
        _returnData = Address.functionCall(proxy, abi.encodeWithSignature(
            "transferERC20(address,address,uint256)",
             address(erc20), address(4), sendERC20Amount / 4 
        ));
        vm.stopPrank();
        assertEq(erc20.balanceOf(address(4)), sendERC20Amount / 4 );     
        assertEq(erc20.balanceOf(proxy), sendERC20Amount * 3 / 4 );  

        // silent period is ended.
        // inheriter entered into the rights
        // creator withdraw money from the trust 
        // check recipient balance before
        assertEq(erc20.balanceOf(address(5)), 0);
        vm.prank(address(1)); 
        _returnData = Address.functionCall(proxy, abi.encodeWithSignature(
            "transferERC20(address,address,uint256)",
             address(erc20), address(5), sendERC20Amount / 4 
        ));
        // check recipient balance after
        assertEq(erc20.balanceOf(address(5)), sendERC20Amount / 4);

        // outsider tries to withdraw money from the trust after silent period
        vm.prank(address(6)); 
        vm.expectRevert(
            abi.encodeWithSelector(DeTrustModel_00.OwnableUnauthorizedAccount.selector, address(6))
        );
        _returnData = Address.functionCall(proxy, abi.encodeWithSignature(
            "transferERC20(address,address,uint256)",
             address(erc20), address(6), sendERC20Amount / 4 
        ));
        // check recipient balance after
        assertEq(erc20.balanceOf(address(6)), 0);

        vm.startPrank(address(1)); 
        // try to withdraw ethers more than prosy has
        vm.expectRevert(
            abi.encodeWithSelector(AddressInsufficientBalance.selector, address(proxy))
        );
        _returnData = Address.functionCall(proxy, abi.encodeWithSignature(
            "transferNative(address,uint256)",
            address(4), sendEtherAmount * 10 
        ));

    }

    function test_iAmAlive() public {
        
        // add balance for msg.sender
        erc20.transfer(address(1), neededERC20Amount);
        
        vm.deal(address(1), 100 ether); 
        vm.startPrank(address(1));
        address proxy = factory.deployProxyForTrust(
            address(impl_00), 
            address(1), //msg.sender,
            keccak256(abi.encode(address(2))), // inheriter
            silentPeriod,
            detrustName
        );

        // move time
        vm.warp(block.timestamp + 100);
        // creator calls iAmAlive()
        bytes memory _returnData = Address.functionCall(proxy, abi.encodeWithSignature(
            "iAmAlive()"
        ));
        vm.stopPrank();

        ITrustModel_00.DeTrustModelStorage_00 memory proxyInfo = ITrustModel_00(proxy).trustInfo();
        assertEq(proxyInfo.lastOwnerOp, block.timestamp);
        assertEq(proxyInfo.inherited, false);

        // move time again
        vm.warp(block.timestamp + 100);
        vm.prank(address(2));
        // inheriter call iAmAlive
        vm.expectRevert(
            abi.encodeWithSelector(DeTrustModel_00.OwnableUnauthorizedAccount.selector, address(2))
        );

        _returnData = Address.functionCall(proxy, abi.encodeWithSignature(
            "iAmAlive()"
        ));

        // move time again - silent period is ended
        // inheriter calls iAmAlive() - nothing will happend
        vm.warp(block.timestamp + silentPeriod + 1);
        vm.prank(address(2));
        _returnData = Address.functionCall(proxy, abi.encodeWithSignature(
            "iAmAlive()"
        ));

        ITrustModel_00.DeTrustModelStorage_00 memory inheritedProxyInfo = ITrustModel_00(proxy).trustInfo();
        assertEq(inheritedProxyInfo.lastOwnerOp, proxyInfo.lastOwnerOp);
        assertEq(inheritedProxyInfo.inherited, true);

    }

    function test_checkRegistry() public {
        
        // add balance for msg.sender
        erc20.transfer(address(1), neededERC20Amount);
        
        vm.deal(address(1), 100 ether); 
        vm.startPrank(address(1));
        address proxy = factory.deployProxyForTrust(
            address(impl_00), 
            address(1), //msg.sender,
            keccak256(abi.encode(address(2))), // inheriter
            silentPeriod,
            detrustName
        );

        address proxy1 = factory.deployProxyForTrust(
            address(impl_00), 
            address(1), //msg.sender,
            keccak256(abi.encode(address(2))), // inheriter
            silentPeriod,
            detrustName
        );

        address proxy2 = factory.deployProxyForTrust(
            address(impl_00), 
            address(1), //msg.sender,
            keccak256(abi.encode(address(3))), // inheriter
            silentPeriod,
            detrustName
        );

        address proxy3 = factory.deployProxyForTrust(
            address(impl_00), 
            address(1), //msg.sender,
            keccak256(abi.encode(address(3))), // inheriter
            silentPeriod,
            detrustName
        );

        assertEq(userReg.getCreatorTrusts(address(1))[0], address(proxy));
        assertEq(userReg.getCreatorTrusts(address(1))[1], address(proxy1));
        assertEq(userReg.getCreatorTrusts(address(1))[2], address(proxy2));
        assertEq(userReg.getCreatorTrusts(address(1))[3], address(proxy3));

        assertEq(userReg.getInheritorTrusts(address(2))[0], address(proxy));
        assertEq(userReg.getInheritorTrusts(address(2))[1], address(proxy1));
        assertEq(userReg.getInheritorTrusts(address(3))[0], address(proxy2));
        assertEq(userReg.getInheritorTrusts(address(3))[1], address(proxy3));

        vm.expectRevert("NonAuthorized factory");
        userReg.registerTrust(address(10), address(1), keccak256(abi.encode(address(3))));
        vm.expectRevert(
            abi.encodeWithSelector(DeTrustModel_00.OwnableUnauthorizedAccount.selector, address(1))
        );
        userReg.setFactoryState(address(10), true);

        vm.stopPrank();

    }

}