// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.13;

import {Test, console2} from "forge-std/Test.sol";
import {Address} from "@openzeppelin/contracts/utils/Address.sol";

//import {DeTrustFactory} from "../src/DeTrustFactory.sol";
import {MockERC20} from "../src/mock/MockERC20.sol";
import {DeTrustModel_01_Executive} from "../src/DeTrustModel_01_Executive.sol";

contract DeTrustModel_01_ExecutiveTest_m_01 is Test {
    uint256 public sendEtherAmount = 1e18;
    uint256 public sendERC20Amount = 2e18;
    string public detrustName = 'NameOfDeTrust';
    DeTrustModel_01_Executive public impl_01;


    MockERC20 public erc20;

    receive() external payable virtual {}
    function setUp() public {
        //factory = new DeTrustFactory(address(0), address(0));
        impl_01 = new DeTrustModel_01_Executive();
        erc20 = new MockERC20('Mock ERC20 Token', 'MOCK');
        bytes32[] memory inheritorHases = new bytes32[](1);
        inheritorHases[0] = keccak256(abi.encodePacked(address(3)));
        impl_01.initialize(
            address(this), // _creator
            inheritorHases,
            1,             // _silence,
            detrustName,   // _name,
            address(0),    // _feeToken,
            0,             // _feeAmount,
            address(0)     // _feeBeneficiary
        );
    }

    function test_gettrustInfo() public {
        console2.log("block.timestamp: %s, msg.sender= %s", block.timestamp, msg.sender);
        DeTrustModel_01_Executive.DeTrustModelStorage_01 memory tr = impl_01.trustInfo_01();
        assertEq(tr.creator, address(this));
        assertEq(tr.name, detrustName);
        assertEq(tr.inheritorHashes.length, 1);
        assertEq(erc20.balanceOf(address(this)), erc20.totalSupply());
    }

    function test_execute() public {
        erc20.transfer(address(impl_01), 1_000e18);
        assertEq(erc20.balanceOf(address(impl_01)), 1_000e18);

        DeTrustModel_01_Executive.DeTrustModelStorage_01 memory tr = impl_01.trustInfo_01();
        
        // Example exec approve
        bytes memory payload = abi.encodeWithSelector(erc20.approve.selector, address(1), 1_000e18);
        impl_01.executeOp(address(erc20), 0, payload);
        assertEq(erc20.allowance(address(impl_01), address(1)), 1_000e18);
        
        // Example exec transfer
        payload = abi.encodeWithSelector(erc20.transfer.selector, address(1), 1_000e18);
        impl_01.executeOp(address(erc20), 0, payload);
        assertEq(erc20.balanceOf(address(1)), 1_000e18);
    }

    // https://solidity-by-example.org/abi-encode/
    function test_execute_textEncode() public {
        erc20.transfer(address(impl_01), 1_000e18);
        assertEq(erc20.balanceOf(address(impl_01)), 1_000e18);

        // Example exec approve
        bytes memory payload = abi.encodeWithSignature("transfer(address,uint256)", address(2), 1_000e18);
        impl_01.executeOp(address(erc20), 0, payload);
        assertEq(erc20.balanceOf(address(2)), 1_000e18);
    }   

    // function test_proxyAddress() public {
    //     address proxy = factory.deployProxyForTrust(
    //         address(impl_00), 
    //         msg.sender,
    //         keccak256(abi.encode(address(0))), 
    //         0,
    //         detrustName
    //     );
    //     console2.log("Implementation   addr: %s", address(impl_00));
    //     console2.log("Proxy for     impl_00: %s", proxy);
    //     assertFalse(address(impl_00) == proxy);
    //     //assertEq(address(impl_00), proxy);
    // }


    // function test_erc20Balance() public {

    //     address proxy = factory.deployProxyForTrust(
    //         address(impl_00), 
    //         msg.sender,
    //         keccak256(abi.encode(address(0))), 
    //         0,
    //         detrustName
    //     );

    //     ITrustModel_00.DeTrustModelStorage memory s = ITrustModel_00(proxy).trustInfo();
    //     console2.log("Trust creator: %s", s.creator);
    //     console2.log("Last op: %s", s.lastOwnerOp);
    //     console2.log("ST: %s", s.silenceTime);
    //     console2.log("inherited: %s", s.inherited);

    //     assertEq(erc20.balanceOf(proxy), 0);
    //     assertEq(erc20.balanceOf(address(this)), erc20.totalSupply());
    //     erc20.transfer(proxy, sendERC20Amount);
    //     assertEq(erc20.balanceOf(proxy), sendERC20Amount);
    //     vm.prank(msg.sender);
    //     bytes memory _returnData = Address.functionCall(proxy, abi.encodeWithSignature(
    //         "transferERC20(address,address,uint256)",
    //         address(erc20), address(this), sendERC20Amount/2
    //     ));
    //     assertEq(erc20.balanceOf(proxy), sendERC20Amount/2);

    // }

    // function test_etherBalance() public {

    //     address proxy = factory.deployProxyForTrust(
    //         address(impl_00), 
    //         msg.sender,
    //         keccak256(abi.encode(address(0))), 
    //         0,
    //         detrustName
    //     );
    //     console2.log("Implementation   addr: %s", address(impl_00));
    //     console2.log("Proxy for     impl_00: %s", proxy);
    //     console2.log("Msg.sender in    test: %s", msg.sender);
    //     console2.log("Test contract address: %s", msg.sender);
    //     console2.log("ETH balnce of Test contract: %s", address(this).balance);
    //     assertEq(proxy.balance, 0);
    //     assertFalse(address(this).balance == 0);
    //     address payable _receiver = payable(proxy);
    //     _receiver.transfer(sendEtherAmount);
    //     assertEq(address(proxy).balance, sendEtherAmount);
    //     bytes memory _returnData = Address.functionStaticCall(proxy, abi.encodeWithSignature(
    //         "creator()"
    //     ));
    //     address _creatorFromCall = address(uint160(uint256(bytes32(_returnData))));
    //     console2.log("Creator from staticcal: %s", _creatorFromCall);
    //     console2.log("ETH balnce of Test contract: %s", address(this).balance);
    //     vm.prank(msg.sender);
    //     _returnData = Address.functionCall(proxy, abi.encodeWithSignature(
    //         "transferNative(address,uint256)",
    //         _creatorFromCall, sendEtherAmount/2
            
    //     ));
    //     assertEq(address(proxy).balance, sendEtherAmount/2);

    // }
}
