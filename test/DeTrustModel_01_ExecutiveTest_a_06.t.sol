// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.13;

import {Test, console2} from "forge-std/Test.sol";
import {Address} from "@openzeppelin/contracts/utils/Address.sol";
import "forge-std/console.sol";

import {DeTrustFactory} from "../src/DeTrustFactory.sol";
import {DeTrustModel_01_Executive} from "../src/DeTrustModel_01_Executive.sol";
import {MockERC20} from "../src/mock/MockERC20.sol";

import {UsersDeTrustRegistry} from "../src/UsersDeTrustRegistry.sol";
import {DeTrustModelRegistry} from "../src/DeTrustModelRegistry.sol";

import {ITrustModel_00} from "../src/interfaces/ITrustModel_00.sol";

contract DeTrustModel_01_ExecutiveTest_a_06 is Test {
    uint256 public sendEtherAmount = 1e18;
    uint256 public sendERC20Amount = 2e18;
    uint256 public feeAmount = 5e18;
    uint64 public silentPeriod = 10000;
    string public detrustName = 'NameOfDeTrust';
    string public badDetrustName = 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA';
    error AddressInsufficientBalance(address account);

    DeTrustFactory  public factory;
    DeTrustModel_01_Executive public impl_01;
    UsersDeTrustRegistry public userReg;
    DeTrustModelRegistry public modelReg;
    bytes32[] inheritorHashes = new bytes32[](10);

    MockERC20 public erc20;

    receive() external payable virtual {}
    function setUp() public {
        impl_01 = new DeTrustModel_01_Executive();
        erc20 = new MockERC20('UBDN token', 'UBDN');
        modelReg = new DeTrustModelRegistry(address(100)); //(msg.sender);
        userReg = new UsersDeTrustRegistry();
        factory = new DeTrustFactory(address(modelReg), address(userReg));

        // with fee to create trust
        modelReg.setModelState(
            address(impl_01),
            DeTrustModelRegistry.TrustModel(0x05, address(0), 0, address(erc20), feeAmount)
        );
        console.logBytes1(modelReg.isModelEnable(address(impl_01), address(0)));

        userReg.setFactoryState(address(factory), true);
        assertEq(
            uint8(modelReg.isModelEnable(address(impl_01), address(0))), 
            uint8(0x05)
        );
        // prepare inheritor's hashes - 10 inheritors
        //bytes32[] memory inheritorHashes = new bytes32[](10);
        for (uint160 i = 0; i < 10; i++) {
            inheritorHashes[i] =  keccak256(abi.encode(address(i)));
        }
    }

    function test_proxy_annual_fee_iAmAlive() public {
        assertEq(address(factory.modelRegistry()), address(modelReg));
        assertEq(address(factory.trustRegistry()), address(userReg));

        // add balance for msg.sender
        erc20.transfer(address(11), feeAmount);
        vm.startPrank(address(11));
        erc20.approve(address(modelReg), feeAmount);
        uint256 balanceBeforeBen = erc20.balanceOf(address(100)); // fee beneficiary balance
        address proxy = factory.deployProxyForTrust(
            address(impl_01), 
            address(11),  // _creator
            inheritorHashes, // _inheritorHashes
            silentPeriod,    //_silence
            detrustName     //_name
        );
        vm.stopPrank();


        // get proxy info
        bytes memory _data = abi.encodeWithSelector(DeTrustModel_01_Executive.trustInfo_01.selector);
        bytes memory _returnData = Address.functionStaticCall(proxy, _data);
        DeTrustModel_01_Executive.DeTrustModelStorage_01 memory proxyInfo = abi.decode(
            _returnData, 
            (DeTrustModel_01_Executive.DeTrustModelStorage_01));
        
        _returnData = Address.functionStaticCall(proxy, abi.encodeWithSignature(
            "ANNUAL_FEE_PERIOD()"
        ));
        uint256 annual_fee_period = uint256(bytes32(_returnData));
        console2.log("ANNUAL_FEE_PERIOD = %s: ", annual_fee_period);
        assertEq(proxyInfo.fee.payedTill, uint64(block.timestamp) + annual_fee_period);

        // move time - time to pay annual fee
        vm.warp(uint64(block.timestamp) + annual_fee_period + 1);
        // check isAnnualFeePayed before the payment - expect false
        _returnData = Address.functionStaticCall(proxy, abi.encodeWithSignature(
            "isAnnualFeePayed()"
        ));
        assertEq(_returnData, abi.encode(false));

        // calling iAmAlive by inheritor - update inherited
        erc20.transfer(address(1), feeAmount);
        vm.startPrank(address(1));
        erc20.transfer(address(proxy), feeAmount);
        balanceBeforeBen = erc20.balanceOf(address(100)); // fee beneficiary balance
        _returnData = Address.functionCall(proxy, abi.encodeWithSignature(
            "iAmAlive()"
        )); 
        vm.stopPrank();
        _returnData = Address.functionStaticCall(proxy, _data);
        DeTrustModel_01_Executive.DeTrustModelStorage_01 memory proxyInfo1 = abi.decode(
            _returnData, 
            (DeTrustModel_01_Executive.DeTrustModelStorage_01));
        assertEq(proxyInfo.fee.payedTill + annual_fee_period, proxyInfo1.fee.payedTill);
        assertEq(proxyInfo1.inherited, true);
        assertEq(proxyInfo1.lastOwnerOp, proxyInfo.lastOwnerOp);

        // check fee payment
        assertEq(erc20.balanceOf(address(100)), balanceBeforeBen + feeAmount);
        assertEq(erc20.balanceOf(address(proxy)), 0);

        // calling iAmAlive by creator - nothing will happend
        // without fee payment
        vm.startPrank(address(11));
        balanceBeforeBen = erc20.balanceOf(address(100)); // fee beneficiary balance
        _returnData = Address.functionCall(proxy, abi.encodeWithSignature(
            "iAmAlive()"
        )); 
        vm.stopPrank();
        assertEq(erc20.balanceOf(address(100)), balanceBeforeBen);
        _returnData = Address.functionStaticCall(proxy, _data);
        DeTrustModel_01_Executive.DeTrustModelStorage_01 memory proxyInfo2 = abi.decode(
            _returnData, 
            (DeTrustModel_01_Executive.DeTrustModelStorage_01));
        assertEq(proxyInfo2.inherited, true); // not changed
        assertEq(proxyInfo2.lastOwnerOp, proxyInfo1.lastOwnerOp); // not changed
    }

}
