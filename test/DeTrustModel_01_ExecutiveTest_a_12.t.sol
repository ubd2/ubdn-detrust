// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.13;

import {Test, console2} from "forge-std/Test.sol";
import {Address} from "@openzeppelin/contracts/utils/Address.sol";
import "forge-std/console.sol";

import {DeTrustFactory} from "../src/DeTrustFactory.sol";
import {DeTrustModel_01_Executive} from "../src/DeTrustModel_01_Executive.sol";
import {MockERC20} from "../src/mock/MockERC20.sol";

import {UsersDeTrustRegistry} from "../src/UsersDeTrustRegistry.sol";
import {DeTrustModelRegistry} from "../src/DeTrustModelRegistry.sol";

import {ITrustModel_00} from "../src/interfaces/ITrustModel_00.sol";

// need pay fee and have enough token balance
contract DeTrustModel_01_ExecutiveTest_a_12 is Test {
    uint256 public sendEtherAmount = 1e18;
    uint256 public sendERC20Amount = 2e18;
    uint256 public feeAmount = 5e18;
    uint256 public requiredBalance = 6e18;
    uint64 public silentPeriod = 10000;
    string public detrustName = 'NameOfDeTrust';
    string public badDetrustName = 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA';
    error AddressInsufficientBalance(address account);

    DeTrustFactory  public factory;
    DeTrustModel_01_Executive public impl_01;
    UsersDeTrustRegistry public userReg;
    DeTrustModelRegistry public modelReg;
    bytes32[] inheritorHashes = new bytes32[](10);

    MockERC20 public erc20;

    receive() external payable virtual {}
    function setUp() public {
        impl_01 = new DeTrustModel_01_Executive();
        erc20 = new MockERC20('UBDN token', 'UBDN');
        modelReg = new DeTrustModelRegistry(address(100)); //(msg.sender);
        userReg = new UsersDeTrustRegistry();
        factory = new DeTrustFactory(address(modelReg), address(userReg));

        // with fee and required balance to create trust
        modelReg.setModelState(
            address(impl_01),
            DeTrustModelRegistry.TrustModel(0x07, address(erc20), requiredBalance, address(erc20), feeAmount)
        );
        console.logBytes1(modelReg.isModelEnable(address(impl_01), address(0)));

        userReg.setFactoryState(address(factory), true);
        assertEq(
            uint8(modelReg.isModelEnable(address(impl_01), address(0))), 
            uint8(0x07)
        );
        // prepare inheritor's hashes - 10 inheritors
        //bytes32[] memory inheritorHashes = new bytes32[](10);
        for (uint160 i = 0; i < 10; i++) {
            inheritorHashes[i] =  keccak256(abi.encode(address(i)));
        }
    }

    function test_proxy() public {
        assertEq(address(factory.modelRegistry()), address(modelReg));
        assertEq(address(factory.trustRegistry()), address(userReg));

        // send transaction from address(1) - balance is zero, expect revert

        vm.prank(address(11));
        vm.expectRevert('Too low Balance'); // there is not enough balance to pay fee

        address proxy = factory.deployProxyForTrust(
            address(impl_01), 
            address(11),  // _creator
            inheritorHashes, // _inheritorHashes
            silentPeriod,    //_silence
            detrustName     //_name
        );

        erc20.transfer(address(11), requiredBalance);
        vm.prank(address(11));
        vm.expectRevert();

        proxy = factory.deployProxyForTrust(
            address(impl_01), 
            address(11),  // _creator
            inheritorHashes, // _inheritorHashes
            silentPeriod,    //_silence
            detrustName     //_name
        );

        erc20.transfer(address(11), feeAmount);
        vm.startPrank(address(11));
        erc20.approve(address(modelReg), feeAmount);
        uint256 balanceBefore = erc20.balanceOf(address(100)); // fee beneficiary balance
        proxy = factory.deployProxyForTrust(
            address(impl_01), 
            address(11),  // _creator
            inheritorHashes, // _inheritorHashes
            silentPeriod,    //_silence
            detrustName     //_name
        );
        assertEq(erc20.balanceOf(address(11)), requiredBalance);
        assertEq(erc20.balanceOf(address(100)), balanceBefore + feeAmount);
    }
}