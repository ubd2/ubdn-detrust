// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.13;

import {Test, console2, Vm} from "forge-std/Test.sol";
import "forge-std/console.sol";
import {Address} from "@openzeppelin/contracts/utils/Address.sol";

import {DeTrustFactory} from "../src/DeTrustFactory.sol";
import {DeTrustModel_00} from "../src/DeTrustModel_00.sol";
import {MockERC20} from "../src/mock/MockERC20.sol";

import {UsersDeTrustRegistry} from "../src/UsersDeTrustRegistry.sol";
import {DeTrustModelRegistry} from "../src/DeTrustModelRegistry.sol";


contract FactoryTest_m_03 is Test {
    uint256 public sendEtherAmount = 1e18;
    uint256 public sendERC20Amount = 2e18;
    uint256 public neededERC20Amount = 100e18;
    string public detrustName = 'NameOfDeTrust';

    DeTrustFactory  public factory;
    DeTrustModel_00 public impl_00;
    UsersDeTrustRegistry public userReg;
    DeTrustModelRegistry public modelReg;
    DeTrustModel_00 public implNotRegistred;

    MockERC20 public erc20;

    receive() external payable virtual {}
    function setUp() public {
        modelReg = new DeTrustModelRegistry(msg.sender);
        userReg = new UsersDeTrustRegistry();
        factory = new DeTrustFactory(address(modelReg), address(userReg));
        impl_00 = new DeTrustModel_00();
        implNotRegistred = new DeTrustModel_00();
        erc20 = new MockERC20('UBDN Mock ERC20 Token', 'UBDN');
        modelReg.setModelState(
            address(impl_00),
            DeTrustModelRegistry.TrustModel(0x03, address(erc20), neededERC20Amount, address(0), 0)
        );
        userReg.setFactoryState(address(factory), true);
    }


    function test_createProxy() public {
        address proxy = factory.deployProxyForTrust(
            address(impl_00), 
            msg.sender,
            keccak256(abi.encode(address(0))), 
            0,
            detrustName
        );
        proxy = factory.deployProxyForTrust(
            address(impl_00), 
            msg.sender,
            keccak256(abi.encode(address(0))), 
            0,
            detrustName
        );

        address[] memory t =  userReg.getCreatorTrusts(address(this));
        assertEq(t.length, 2);
        
        address[] memory intr =  userReg.getInheritorTrusts(address(0));
        assertEq(intr.length, 2);
    }

    function test_impNotRegistred() public {
        vm.expectRevert("Model not approved");
        address proxy = factory.deployProxyForTrust(
                address(implNotRegistred), 
                msg.sender,
                keccak256(abi.encode(address(0))), 
                0,
                detrustName
            );
    }

    function test_checkEvents() public {
        // add balance for msg.sender
        erc20.transfer(address(1), neededERC20Amount);
        vm.prank(address(1));
        vm.recordLogs();
        address proxy = factory.deployProxyForTrust(
                address(impl_00), 
                address(1),
                keccak256(abi.encode(address(2))), 
                0,
                detrustName
            );

        Vm.Log[] memory entries = vm.getRecordedLogs();
        assertEq(entries.length, 3);
        assertEq(entries[0].topics[0], keccak256("Upgraded(address)"));
        assertEq(entries[1].topics[0], keccak256("Initialized(uint64)"));
        assertEq(entries[2].topics[0], keccak256("NewTrust(address,address,address,string)"));
        
    }

    function test_getModelList() public {
        address[] memory ml = modelReg.getModelsList();
        assertEq(ml.length, 1);
        modelReg.setModelState(
            address(18),
            DeTrustModelRegistry.TrustModel(0x03, address(erc20), neededERC20Amount, address(0), 0)
        );
        
        ml = modelReg.getModelsList();
        assertEq(ml.length, 2);
        
        modelReg.removeModel(address(impl_00));
        ml = modelReg.getModelsList();
        assertEq(ml.length, 1);
    }
}
