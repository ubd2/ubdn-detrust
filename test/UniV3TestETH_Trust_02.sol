// SPDX-License-Identifier: MIT
pragma solidity ^0.8.20;

import "forge-std/Test.sol";
import "forge-std/console.sol";

import {DeTrustFactory} from "../src/DeTrustFactory.sol";
import {DeTrustModel_01_Executive} from "../src/DeTrustModel_01_Executive.sol";
import {DeTrustModelRegistry} from "../src/DeTrustModelRegistry.sol";
import {UsersDeTrustRegistry} from "../src/UsersDeTrustRegistry.sol";


import {ISwapRouter} from'@uniswap/v3-periphery/contracts/interfaces/ISwapRouter.sol';
//import {INonfungiblePositionManager} from'@uniswap/v3-periphery/contracts/interfaces/INonfungiblePositionManager.sol';
import '@uniswap/v3-periphery/contracts/interfaces/IQuoter.sol';
import '@uniswap/v3-periphery/contracts/libraries/TransferHelper.sol';
import {IERC20} from "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import {Address} from "@openzeppelin/contracts/utils/Address.sol";


// ETH Mainnet addresses
address constant WETH = 0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2;
address constant DAI = 0x6B175474E89094C44Da98b954EedeAC495271d0F;
//address constant USDT = 0xdAC17F958D2ee523a2206206994597C13D831ec7;
address constant WBTC = 0x2260FAC5E5542a773Aa44fBCfeDf7C193bc2C599;
address constant USDC = 0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48;

//address constant Factory = 0xB9097ef1c733824d21e52FACe124f65A1a61b984;
//address constant Impl_01 = 0xE691b58fe9ff159e35d68D0783f8044203292782;
address constant SWAP_ROUTER = 0xE592427A0AEce92De3Edee1F18E0157C05861564;
address constant UNI_QUOTER = 0xb27308f9F90D607463bb33eA1BeBb41C27CE5AB6;
address constant PositionManager = 0xC36442b4a4522E871399CD717aBDD847Ab11FE88;

// from Uniswap V3 interface
struct MintParams {
        address token0;
        address token1;
        uint24 fee;
        int24 tickLower;
        int24 tickUpper;
        uint256 amount0Desired;
        uint256 amount1Desired;
        uint256 amount0Min;
        uint256 amount1Min;
        address recipient;
        uint256 deadline;
    }

struct Position {
        uint96 nonce;
        address operator;
        address token0;
        address token1;
        uint24 fee;
        int24 tickLower;
        int24 tickUpper;
        uint128 liquidity;
        uint256 feeGrowthInside0LastX128;
        uint256 feeGrowthInside1LastX128;
        uint128 tokensOwed0;
        uint128 tokensOwed1;
    }

// the trust uses adding liquidity in Uniswap v3
contract UniV3TestETH_Trust_02 is Test {

    IERC20 private dai = IERC20(DAI);
    IERC20 private wbtc = IERC20(WBTC);
    IERC20 private weth = IERC20(WETH);
    //IERC20 private usdt = IERC20(USDT);
    IERC20 private usdc = IERC20(USDC);
    uint256 public feeAmount = 5000000;
    uint256 public sendERC20AmountUsdc = 100e6;
    uint256 public sendERC20AmountDAI = 100e18;
    uint256 public sendERC20AmountWETH = 1e18;
    uint64 public silentPeriod = 10000;
    string public detrustName = 'NameOfDeTrust';
    bytes32[] inheritorHashes = new bytes32[](10);
    uint24 protocolFee = 3000;
    uint160 sqrtPriceLimitX96 = 0;

    int24 private constant MIN_TICK = -887272;
    int24 private constant MAX_TICK = -MIN_TICK;
    int24 private constant TICK_SPACING = 60;

    DeTrustFactory  public factory;
    DeTrustModel_01_Executive public impl_01;
    UsersDeTrustRegistry public userReg;
    DeTrustModelRegistry public modelReg;
    ISwapRouter public constant router = ISwapRouter(SWAP_ROUTER);

    function setUp() public {
        //DeTrustFactory factory = DeTrustFactory(Factory);
        //DeTrustModel_01_Executive impl_01 = DeTrustModel_01_Executive(Impl_01);
        impl_01 = new DeTrustModel_01_Executive();
        modelReg = new DeTrustModelRegistry(address(100)); //(msg.sender);
        factory = new DeTrustFactory(address(modelReg), address(userReg));
        userReg = new UsersDeTrustRegistry();

        
        modelReg.setModelState(
            address(impl_01),
            DeTrustModelRegistry.TrustModel(0x05, 
                address(0), 
                0, 
                address(USDC), 
                feeAmount)
        );
        userReg.setFactoryState(address(factory), true);
        // prepare inheritor's hashes - 10 inheritors
        //bytes32[] memory inheritorHashes = new bytes32[](10);
        for (uint160 i = 0; i < 10; i++) {
            inheritorHashes[i] =  keccak256(abi.encode(address(i)));
        }

        // swap eth to usdc in Uniswap v3 to work
        ISwapRouter.ExactInputSingleParams memory params = ISwapRouter
            .ExactInputSingleParams({
                tokenIn: WETH,
                tokenOut: USDC, 
                fee: uint24(protocolFee),
                recipient: address(this),
                deadline: block.timestamp,
                amountIn: 1e18,
                amountOutMinimum: 0,
                sqrtPriceLimitX96: sqrtPriceLimitX96
        });

        uint256 amountOut = router.exactInputSingle{value: 1e18}(params);

        // swap eth to dai in Uniswap v3 to work
        params = ISwapRouter
            .ExactInputSingleParams({
                tokenIn: WETH,
                tokenOut: DAI, 
                fee: uint24(protocolFee),
                recipient: address(this),
                deadline: block.timestamp,
                amountIn: 1e18,
                amountOutMinimum: 0,
                sqrtPriceLimitX96: sqrtPriceLimitX96
        });

        amountOut = router.exactInputSingle{value: 1e18}(params);
    }

    function test_Proxy() public {
        // deploy trust
        TransferHelper.safeApprove(USDC, address(modelReg), feeAmount);
        uint256 balanceBeforeBen = usdc.balanceOf(address(100)); // fee beneficiary balance
        address proxy = factory.deployProxyForTrust(
            address(impl_01), 
            address(11),  // _creator
            inheritorHashes, // _inheritorHashes
            silentPeriod,    //_silence
            detrustName     //_name
        );

        // topup trust
        address payable _receiver = payable(proxy);
        _receiver.transfer(sendERC20AmountWETH);
        bytes memory payload = abi.encodeWithSignature("deposit()");
        vm.prank(address(11));
        bytes memory _returnData = Address.functionCall(proxy, abi.encodeWithSignature(
            "executeOp(address,uint256,bytes)",
            weth,1e18,payload
        ));
        assertEq(weth.balanceOf(proxy), sendERC20AmountWETH);

        TransferHelper.safeTransfer(DAI, proxy, sendERC20AmountDAI);
        assertEq(dai.balanceOf(proxy), sendERC20AmountDAI);

        // make approves for PositionManager
        vm.startPrank(address(11));

        payload = abi.encodeWithSignature("approve(address,uint256)", PositionManager, sendERC20AmountDAI);

        _returnData = Address.functionCall(proxy, abi.encodeWithSignature(
            "executeOp(address,uint256,bytes)",
            DAI,0,payload
        ));
        assertEq(dai.allowance(proxy, PositionManager), sendERC20AmountDAI);

        payload = abi.encodeWithSignature("approve(address,uint256)", PositionManager, sendERC20AmountWETH);

        _returnData = Address.functionCall(proxy, abi.encodeWithSignature(
            "executeOp(address,uint256,bytes)",
            WETH,0,payload
        ));
        assertEq(weth.allowance(proxy, PositionManager), sendERC20AmountWETH);

        MintParams memory params = MintParams({
            token0: DAI,
            token1: WETH,
            fee: protocolFee,
            tickLower: (MIN_TICK / TICK_SPACING) * TICK_SPACING,
            tickUpper: (MAX_TICK / TICK_SPACING) * TICK_SPACING,
            amount0Desired: sendERC20AmountDAI,
            amount1Desired: sendERC20AmountWETH,
            amount0Min: 0,
            amount1Min: 0,
            recipient: proxy, // address(this),
            deadline: block.timestamp
        });
    
        payload = abi.encodeWithSignature("mint((address,address,uint24,int24,int24,uint256,uint256,uint256,uint256,address,uint256))", params);
        _returnData = Address.functionCall(proxy, abi.encodeWithSignature(
            "executeOp(address,uint256,bytes)",
            PositionManager,0,payload
        ));

        _returnData = Address.functionStaticCall(PositionManager, abi.encodeWithSignature(
            "tokenOfOwnerByIndex(address,uint256)",proxy,0
        ));
        uint256 tokenId = uint256(bytes32(_returnData));

        _returnData = Address.functionStaticCall(PositionManager, abi.encodeWithSignature(
            "positions(uint256)",tokenId
        ));
        
        Position memory position = abi.decode(
            _returnData, 
            (Position));
        assertEq(position.token0, DAI);
        assertEq(position.token1, WETH);

    }
}